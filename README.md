# FOSYMA : Hunt the Wumpus
MAS project, forked from Dedale, based on the game "Hunt the Wumpus" (G. Yob, 1972).
## Install
  Check the [Dedale](https://dedale.gitlab.io/) website for installation guidelines and documentation.
## Description
Agents must explore a graph and collect *gold* and *diamond* from treasure chests, being aware of the Wumpus.
### Rules
- Agents can only move through existing edges. 
- Nodes can contain several treasure chests but only a single agent at a time - Wumpus included.
- Agents do not directly interact with the Wumpus but it spills stench around its position.
- Once an agent opened one type of treasure chest (gold or diamond) it must stick to it.
- The Wumpus can move and will move treasure chests around to fool the agents
