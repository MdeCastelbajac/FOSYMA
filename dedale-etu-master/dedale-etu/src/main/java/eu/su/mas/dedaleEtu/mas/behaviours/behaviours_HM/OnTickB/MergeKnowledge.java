package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.OnTickB;

import java.io.IOException;
import java.util.*;

import dataStructures.tuple.Couple;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import org.glassfish.pfl.dynamic.codegen.impl.ExpressionFactory.ThisExpression;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;
import dataStructures.serializableGraph.SerializableSimpleGraph;
/**
 * This example behaviour try to send a hello message (every 3s maximum) to agents Collect2 Collect1
 * @author hc
 *
 */
public class MergeKnowledge extends TickerBehaviour{
	private agent_data data;
	//private AbstractDedaleAgent myagent;
	/**
	 * 
	 */
	private static final long serialVersionUID = -2058134622078521998L;


	private void MaJ_contact_sender(String agent_name){
		this.data.contacts.put(agent_name,0);
	}

	public MergeKnowledge (final Agent myagent,final agent_data data) {
		super(myagent, 1);
		this.data=data;
	}

	private void ask_for_maj(String tmp_rec_name){
		ACLMessage msg_AskMaj = new ACLMessage(ACLMessage.INFORM);
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.setProtocol("AskForMaj");
		msg_AskMaj.setContent("   ");
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.addReceiver(new AID(tmp_rec_name, AID.ISLOCALNAME));
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskMaj);
		MaJ_contact_sender(tmp_rec_name);
	}

	public void mergeWumpusNodes(ArrayList<String> wumpusNodes) {
		for (String node : wumpusNodes) {
			if (! this.data.wumpusNodes.contains(node)) {
				if (!this.data.myMap.getClosedNodes().contains(node)){
					this.data.wumpusNodes.add(node);
					this.data.myMap.addNode(node,MapAttribute.agent);
				}

			}
		}
	}


	public void mergeResources(Hashtable<String, Couple<Resource, Resource>> rReceived) {
		// Merges received resources with current ones
		Set<String> allKeys = rReceived.keySet();
		for(String key: allKeys) {
			// Test if the position is already known 
			if(this.data.resources.containsKey(key)) {
				Couple<Resource, Resource> knownR = this.data.resources.get(key);
				Couple<Resource, Resource> newR = rReceived.get(key);
				
				// Gold
				Resource newGold = newR.getLeft();
				Resource oldGold = knownR.getLeft();
				
				// Diamond
				Resource newDia = newR.getRight();
				Resource oldDia = knownR.getRight();
				if( newGold != null ) {
					if( newGold.date.after(oldGold.date)) {
						// Update Gold
						oldGold.date = newGold.date;
						oldGold.quantity = newGold.quantity;
					}
				}
				if( newDia != null ) {
					if( newDia.date.after(oldDia.date)) {
						// Update Dia
						oldDia.date = newDia.date;
						oldDia.quantity = newDia.quantity;
					}
				}
			}
			// Else just add new resource entry
			else {
				this.data.resources.put(key, rReceived.get(key));
			}
		}
		
	}



	public void mergeFeuilleDeRoute(Hashtable <String,Couple<String,List<String>>> rReceived,String sender) {

		this.data.feuille_de_route=rReceived.get(this.myAgent.getLocalName()).getRight();
		this.data.feuilles_de_route=rReceived;
		this.data.type=rReceived.get(this.myAgent.getLocalName()).getLeft();


		ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
		msg_3.setProtocol("SHARE_FEUILLES-de-ROUTE");
		msg_3.setSender(this.myAgent.getAID());


		Enumeration<String> dico_keys = this.data.contacts.keys();


		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			if (!name.equals(sender)){
				msg_3.addReceiver(new AID(name, AID.ISLOCALNAME));
			}

		}

		try {
			msg_3.setContentObject(rReceived);
		} catch (IOException e) {
			e.printStackTrace();
		}

		if (this.data.interlocking!=0){
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);
		}


		ACLMessage msg_4 = new ACLMessage(ACLMessage.INFORM);
		msg_4.setProtocol("SHARE_FEUILLES-de-ROUTE-OK");
		msg_4.setSender(this.myAgent.getAID());

		msg_4.addReceiver(new AID(sender, AID.ISLOCALNAME));

		msg_4.setContent(" ");
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg_4);

	}


	public void mergeBackPack(Hashtable<String, Couple<Integer, Integer>> rReceived) {

		// Merges received resources with current ones
		Set<String> allKeys = rReceived.keySet();

		for(String key: allKeys) {


			Couple<Integer, Integer> newC = rReceived.get(key);
			this.data.capacities.put(key, newC);

		}

	}

	private ACLMessage ACL_MESSAGE_RECU(String protocole){

		MessageTemplate msgTemplate_merge=MessageTemplate.and(
				MessageTemplate.MatchProtocol(protocole),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived_merge=this.myAgent.receive(msgTemplate_merge);

		return msgReceived_merge;
	}

	private void gestion_MaJ(ACLMessage msgTemplate_ask_r){
		if (msgTemplate_ask_r!=null) {


			if (this.data.contacts.get(msgTemplate_ask_r.getSender().getLocalName())<0||this.data.contacts.get(msgTemplate_ask_r.getSender().getLocalName())>5){
				ask_for_maj(msgTemplate_ask_r.getSender().getLocalName());
				this.data.contacts.put(msgTemplate_ask_r.getSender().getLocalName(),0);
			}

			String receiver= msgTemplate_ask_r.getSender().getLocalName();

			// Shares MAP
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.setProtocol("SHARE-TOPO");
			msg.setSender(this.myAgent.getAID());
			msg.addReceiver(new AID(receiver, AID.ISLOCALNAME));

			SerializableSimpleGraph<String, MapRepresentation.MapAttribute> sg=this.data.myMap.getSerializableGraph();
			try {
				msg.setContentObject(sg);
			} catch (IOException e) {
				e.printStackTrace();
			}
			((AbstractDedaleAgent)this.myAgent).sendMessage(msg);


			// Shares RESOURCES
			if(this.data.resources.size() != 0 ) {

				ACLMessage msg_2 = new ACLMessage(ACLMessage.INFORM);
				msg_2.setProtocol("SHARE-RESOURCES");
				msg_2.setSender(this.myAgent.getAID());
				msg_2.addReceiver(new AID(receiver, AID.ISLOCALNAME));


				Hashtable<String, Couple<Resource, Resource>> sh=this.data.resources;
				try {
					msg_2.setContentObject(sh);
				} catch (IOException e) {
					e.printStackTrace();
				}
				((AbstractDedaleAgent)this.myAgent).sendMessage(msg_2);
			}
			// Shares Wumpus Nodes
			if(this.data.wumpusNodes.size() != 0 ) {

				ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
				msg_3.setProtocol("SHARE-WUMPUS");
				msg_3.setSender(this.myAgent.getAID());
				msg_3.addReceiver(new AID(receiver, AID.ISLOCALNAME));

				ArrayList<String> sl=this.data.wumpusNodes;
				try {
					msg_3.setContentObject(sl);
				} catch (IOException e) {
					e.printStackTrace();
				}
				((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);
			}

		}

	}

	@Override
	public void onTick() {

		
				
				// Map shared ?
		ACLMessage msgReceived_merge =  ACL_MESSAGE_RECU("SHARE-TOPO");

		if (msgReceived_merge!=null) {
			MaJ_contact_sender(msgReceived_merge.getSender().getLocalName());

			SerializableSimpleGraph<String, MapAttribute> sgreceived=null;
			try {
				sgreceived = (SerializableSimpleGraph<String, MapAttribute>)msgReceived_merge.getContentObject();
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			this.data.myMap.mergeMap(sgreceived);
		}
				
		ACLMessage msgReceived_merge_r=ACL_MESSAGE_RECU("SHARE-RESOURCES");

		if (msgReceived_merge_r!=null) {
			MaJ_contact_sender(msgReceived_merge_r.getSender().getLocalName());
			Hashtable<String, Couple<Resource, Resource>> shreceived=null;
			try {
				shreceived = (Hashtable<String, Couple<Resource, Resource>>)msgReceived_merge_r.getContentObject();
			} catch (UnreadableException e) {
						// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mergeResources(shreceived);
		}
				
		// Wumpus Nodes Shared ?
		ACLMessage msgReceived_merge_w=ACL_MESSAGE_RECU("SHARE-WUMPUS");
		if (msgReceived_merge_w!=null) {
			MaJ_contact_sender(msgReceived_merge_w.getSender().getLocalName());
			ArrayList<String> slreceived=null;
			try {
				slreceived = (ArrayList<String>)msgReceived_merge_w.getContentObject();
			} catch (UnreadableException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			mergeWumpusNodes(slreceived);
		}

		ACLMessage msgReceived_merge_BACKPACK=ACL_MESSAGE_RECU("SHARE_BACKPACK-C");
		if (msgReceived_merge_BACKPACK!=null) {

			MaJ_contact_sender(msgReceived_merge_BACKPACK.getSender().getLocalName());
			Hashtable<String, Couple<Integer, Integer>> Breceived=null;
			try {
				Breceived = (Hashtable<String, Couple<Integer, Integer>>)msgReceived_merge_BACKPACK.getContentObject();
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mergeBackPack(Breceived);
		}

		ACLMessage msgReceived_merge_FEUILLE=ACL_MESSAGE_RECU("SHARE_FEUILLES-de-ROUTE");
		if (msgReceived_merge_FEUILLE!=null) {


			Hashtable<String,Couple<String,List<String>>> Breceived=null;
			try {
				Breceived = (Hashtable<String,Couple<String,List<String>>>)msgReceived_merge_FEUILLE.getContentObject();
			} catch (UnreadableException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			mergeFeuilleDeRoute(Breceived,msgReceived_merge_FEUILLE.getSender().getLocalName());
		}

		ACLMessage msgReceived_merge_FEUILLE_OK=ACL_MESSAGE_RECU("SHARE_FEUILLES-de-ROUTE-OK");
		if (msgReceived_merge_FEUILLE_OK!=null) {
			this.data.envoi_des_receveurs.remove(msgReceived_merge_FEUILLE_OK.getSender().getLocalName());

			if (this.data.envoi_des_receveurs.size()<=1) {
				this.data.interlocking = 0;
			}



		}



		ACLMessage msgTemplate_ask_r=ACL_MESSAGE_RECU("AskForMaj");
		gestion_MaJ(msgTemplate_ask_r);



				
				
		}
	}
