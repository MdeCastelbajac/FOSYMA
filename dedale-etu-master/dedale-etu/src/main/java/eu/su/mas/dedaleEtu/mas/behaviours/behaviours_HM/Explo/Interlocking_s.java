package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Explo;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.util.*;


/*
 * Solves Interlocking
 * Gives a (non)priority
 *
 */
public class Interlocking_s extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;


	private String prevnode;
	private int finished;
	private List<String> list_agentNames;
	private AbstractDedaleAgent myagent;
	private agent_data data;
	private int cpt;
	private String trial;
	private int ping_cpt;


	public Interlocking_s(final AbstractDedaleAgent myagent, List<String> agentNames, agent_data data) {
		super(myagent);

		this.finished=0;
		this.list_agentNames=agentNames;
		this.myagent=myagent;
		this.data=data;
		this.prevnode=null;
		this.cpt=0;
		this.ping_cpt=0;

		this.data.chemin_prio=null;
		this.trial=null;

	}


	private void MAJ_carte(){
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

		this.data.nextNode=null;

		Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();

		while(iter.hasNext()){
			String nodeId=iter.next().getLeft();
			boolean isNewNode=this.data.myMap.addNewNode(nodeId);

			if (!Objects.equals(myPosition, nodeId)) {
				this.data.myMap.addEdge(myPosition, nodeId);
				if (this.data.nextNode==null || isNewNode) this.data.nextNode=nodeId;

				if (isNewNode){
					Random rand = new Random();
					int tmp =rand.nextInt(100);
					if (tmp>50){
						this.data.nextNode=nodeId;
					}
				}

			}
		}

		Couple<String,List<Couple<Observation, Integer>>> currentNodeObs=lobs.get(0);

		lobs.remove(0);
		for ( Couple<Observation, Integer> obs : currentNodeObs.getRight()) {

			if (obs.getLeft().getName().equals("Stench") && !this.data.myMap.getClosedNodes().contains(this.myagent.getCurrentPosition()) ) {
				this.data.stench = 1;
			}
		}
		this.data.myMap.addNode(this.myagent.getCurrentPosition(), MapRepresentation.MapAttribute.closed);
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs_2=((AbstractDedaleAgent)this.myAgent).observe();
		for( Couple<String, List<Couple<Observation, Integer>>> obs:lobs_2) {
			// Extracts current observations
			String position = obs.getLeft();
			List<Couple<Observation, Integer>> lo = obs.getRight();
			if (!lo.isEmpty()) {
				Couple<Observation, Integer> o = lo.get(0);
				String type = o.getLeft().getName();
				Integer quantity = o.getRight();

				// Updates data.resources
				if (type.equals("Gold")||type.equals("Diamond")) {
					Resource resource = new Resource();
					resource.type = type;
					resource.quantity = quantity;
					resource.date = new Date();

					if (this.data.resources.containsKey(position)) {
						Couple<Resource, Resource> res = this.data.resources.get(position);
						if(type.equals("Gold")) {
							if (res.getLeft().date.before(resource.date)){
								Resource dia = res.getRight();
								this.data.resources.remove(position);
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, dia);
								this.data.resources.put(position, newRes);
							}
						}else {
							if (res.getRight().date.before(resource.date)){
								Resource gold = res.getLeft();
								this.data.resources.remove(position);
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(gold, resource);
								this.data.resources.put(position, newRes);
							}
						}

					}else {
						if(type.equals("Gold")) {
							Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, null);
							this.data.resources.put(position, newRes);
						}else{
							Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(null, resource);
							this.data.resources.put(position, newRes);
						}
					}
				}
			}

		}
	}


	private void Ping() {


		List<Couple<String, List<Couple<Observation, Integer>>>> lobs = ((AbstractDedaleAgent) this.myAgent).observe();//myPosition

		if (this.data.previous_node_2 == null) {
			return;
		}
		this.ping_cpt += 1;

		if (this.data.previous_node_2.equals(this.data.previous_node_1)) {


			this.ping_cpt = 0;
			String myPosition = this.myagent.getCurrentPosition();

			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.setSender(this.myAgent.getAID());
			msg.setProtocol("Ping");

			String tmp_goal_node = this.data.myMap.goalNode;

			msg.setContent(this.data.interlocuteurpriority + ";" + this.data.interlocuteurNode + ";" + this.data.interlocuteurGoalNode + ";" + this.data.myMap.getOpenNodes().size() + ";" + this.data.myMap.getClosedNodes().size());
			msg.setSender(this.myAgent.getAID());

			Enumeration<String> dico_keys = this.data.contacts.keys();

			int nb_destinataires = 0;

			while (dico_keys.hasMoreElements()) {
				String name = dico_keys.nextElement();
				int n = this.data.contacts.get(name);

				if ((n < 0) || (n > 5)) {
					msg.addReceiver(new AID(name, AID.ISLOCALNAME));
					nb_destinataires += 1;
					this.data.contacts.put(name,0);
				}
			}

			if (nb_destinataires >= 1) {
				((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
			}


		}
	}


	private void fin_interlocking(){
		this.finished=1;
		this.data.interlocking=0;
		this.data.interlocuteurpriority=0;
		this.data.nom_interlocuteur=null;
		//fin de l'interlockage
		this.data.nom_interlocuteur=null;
		this.data.interlocuteurNode=null;
		this.data.priority=0;
		this.cpt=0;
	}

	@Override
	public void action() {
		Ping();

		String myPosition=this.myagent.getCurrentPosition();
		MAJ_carte();




		if (this.cpt>10){
			fin_interlocking();
			return;
		}else{
			this.finished=0;
		}

		this.cpt+=1;

		if (this.data.chemin_prio==null){
			try {
				this.data.chemin_prio=this.data.myMap.getShortestPath(myPosition,this.data.interlocuteurGoalNode);
				this.data.chemin_prio_index=this.data.chemin_prio.indexOf(myPosition);
			}catch(Exception e){
				return ;
			}

			this.prevnode=myPosition;
			if (this.trial==null){
				this.trial=myPosition;
			}


		}


		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition


		Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
		while(iter.hasNext()){
			String nodeId=iter.next().getLeft();


			if (!Objects.equals(myPosition, nodeId)) {
				this.data.myMap.addEdge(myPosition, nodeId);



				if ((((!nodeId.equals(this.data.previous_node_1))&&(!nodeId.equals(this.data.previous_node_2))))&&(!nodeId.equals(this.trial))){
					//System.out.println("nodeID : "+ nodeId + " dom pos: "+ this.data.interlocuteurNode + " chemin? : ");
					if (this.data.nextNode!=null){
						if ((!this.data.chemin_prio.contains(nodeId))&&(!nodeId.equals(this.data.interlocuteurGoalNode))){
							this.data.nextNode=nodeId;


						}

					}else{
						if (!nodeId.equals(trial)) {
							this.data.nextNode = nodeId;
						}
					}



				}


			}
		}

		this.data.previous_node_2=this.data.previous_node_1;
		this.data.previous_node_1=myPosition;
		this.prevnode=myPosition;
		this.trial=this.data.nextNode;

		if (this.data.nextNode!=null){
			((AbstractDedaleAgent)this.myAgent).moveTo(this.data.nextNode);
		}







		Enumeration<String> e_tmp = this.data.contacts.keys();

		while (e_tmp.hasMoreElements()) {
			String key = e_tmp.nextElement();
			Integer n = this.data.contacts.get(key);
			if (n>-1){
				this.data.contacts.put(key,n+1);
			}

		}

		try {
			this.myAgent.doWait(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}


	}



	public int onEnd(){
		return finished;
	}



}