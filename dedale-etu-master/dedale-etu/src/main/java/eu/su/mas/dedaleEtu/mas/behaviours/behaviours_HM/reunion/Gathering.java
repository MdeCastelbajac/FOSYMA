package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.reunion;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.Enumeration;
import java.util.List;
import java.util.Random;


/*
 * Decides whether the agent is the leader of the gathering
 */


public class Gathering extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private int finished;
	private List<String> list_agentNames;
	private AbstractDedaleAgent myagent;
	private agent_data data;

	public Gathering(final AbstractDedaleAgent myagent, List<String> agentNames, agent_data data) {
		super(myagent);

		this.finished=0;
		this.list_agentNames=agentNames;
		this.myagent=myagent;
		this.data=data;
	}


	@Override
	public void action() {
		
		//System.out.println(this.myagent.getLocalName()+" in Gathering");
		this.finished=0;
		try {
			this.myAgent.doWait(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(this.myagent.getCurrentPosition().equals(this.data.rdv_point)) {
			System.out.println(this.myagent.getLocalName()+" dit : je suis le leader.");
			this.finished=1;
		}else {
			System.out.println(this.myagent.getLocalName()+" dit : j'attends les infos de collecte.");
			this.finished=2;
		}

	}


	public int onEnd(){
		return finished;
	}



}