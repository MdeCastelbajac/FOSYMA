package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Explo;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.ExploreCoopAgentHM;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


/**
 * FSM INIT STATE 
 *
 * Observe & updates knowledge
 * Chose next node to move to
 * Spots Interlocking
 */

public class Init extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;


	private int finished;
	private AbstractDedaleAgent myagent;
	private agent_data data;

	private int cpt_interlock_prio;


	public Init(final AbstractDedaleAgent myagent, List<String> agentNames,agent_data data) {
		super(myagent);

		this.finished=0;
		this.myagent=myagent;
		this.data=data;

		this.cpt_interlock_prio=0;

	}

	private void MaJ_contacts(){
		Enumeration<String> e = this.data.contacts.keys();

		while (e.hasMoreElements()) {
			String key = e.nextElement();
			Integer n = this.data.contacts.get(key);
			if (n>=0) {
				this.data.contacts.put(key, n + 1);
			}
		}
	}

	private void Attente(){
		try {
			this.myAgent.doWait(500);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private void Ping(){

		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		if (this.data.previous_node_2==null){return;}



		if (lobs.size()>=4 || this.data.previous_node_2.equals(this.data.previous_node_1) ){
			String myPosition= this.myagent.getCurrentPosition();

			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.setSender(this.myAgent.getAID());
			msg.setProtocol("Ping");

			String tmp_goal_node = this.data.myMap.goalNode;

			if (this.data.previous_node_2 != null) {
				if ((this.data.previous_node_1.equals(this.data.previous_node_2)) && (this.data.previous_node_1.equals(this.myagent.getCurrentPosition()))) {
					Random rand = new Random();
					int tmp_prio=0;

					if (lobs.size() <= 2) {
						tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * 250 + 39999;
					} else {
						tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * 250;
					}

					if (tmp_prio>this.data.priority){
						this.data.priority=tmp_prio;
					}
				}
			}

			msg.setContent(this.data.priority + ";" + myPosition + ";" + tmp_goal_node + ";" + this.data.myMap.getOpenNodes().size() + ";" + this.data.myMap.getClosedNodes().size());
			msg.setSender(this.myAgent.getAID());

			Enumeration<String> dico_keys = this.data.contacts.keys();

			int nb_destinataires = 0;

			while (dico_keys.hasMoreElements()) {
				String name = dico_keys.nextElement();
				int n = this.data.contacts.get(name);

				if ((n < 0) || (n > 5)) {
					msg.addReceiver(new AID(name, AID.ISLOCALNAME));
					nb_destinataires += 1;
					this.data.contacts.put(name,0);
				}
			}

			if (nb_destinataires >= 1) {
				((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
			}
		}
	}



	private void MAJ_carte(){
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

		this.data.nextNode=null;

		Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();

		while(iter.hasNext()){
			String nodeId=iter.next().getLeft();
			boolean isNewNode=false;
			if(( !this.data.myMap.getClosedNodes().contains(nodeId)&&(!this.data.myMap.getAgentNodes().contains(nodeId)))) {
				this.data.myMap.addNode(nodeId, MapAttribute.open);
				isNewNode=true;
			}


			if (!Objects.equals(myPosition, nodeId)) {
				this.data.myMap.addEdge(myPosition, nodeId);
				if (this.data.nextNode==null && isNewNode) this.data.nextNode=nodeId;

				if (isNewNode){
					Random rand = new Random();

					if (this.myagent.getLocalName().equals("Explo2")){
						int tmp =rand.nextInt(100);
						if (tmp>25){
							this.data.nextNode=nodeId;
						}
					}

					if (this.myagent.getLocalName().equals("Explo3")){
						int tmp =rand.nextInt(100);
						if (tmp>50){
							this.data.nextNode=nodeId;
						}
					}

					if (this.myagent.getLocalName().equals("Explo4")){
						this.data.nextNode=nodeId;

					}

				}
			}
		}

		Couple<String,List<Couple<Observation, Integer>>> currentNodeObs=lobs.get(0);


		lobs.remove(0);
		for ( Couple<Observation, Integer> obs : currentNodeObs.getRight()) {
			//bug possible


			//if ((obs.getLeft().getName().equals("Stench"))&&(!obs.getRight().equals(null))) {
			if (obs.getLeft().getName().equals("Stench")) {
				this.data.stench = 1;


			}
		}
		this.data.myMap.addNode(this.myagent.getCurrentPosition(), MapAttribute.closed);
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs_2=((AbstractDedaleAgent)this.myAgent).observe();
		for( Couple<String, List<Couple<Observation, Integer>>> obs:lobs_2) {
			// Extracts current observations
			String position = obs.getLeft();
			List<Couple<Observation, Integer>> lo = obs.getRight();

			if (!lo.isEmpty()) {
				for(Couple<Observation, Integer> o : lo) {
					String type = o.getLeft().getName();
					Integer quantity = o.getRight();

					// Updates data.resources
					if (type.equals("Gold")||type.equals("Diamond")) {
						Resource resource = new Resource();
						resource.type = type;
						resource.quantity = quantity;
						resource.date = new Date();

						if (this.data.resources.containsKey(position)) {
							Couple<Resource, Resource> res = this.data.resources.get(position);
							if(type.equals("Gold")) {
								if (res.getLeft().date.before(resource.date)){
									Resource dia = res.getRight();
									this.data.resources.remove(position);
									Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, dia);
									this.data.resources.put(position, newRes);
								}
							}else {
								if (res.getRight().date.before(resource.date)){
									Resource gold = res.getLeft();
									this.data.resources.remove(position);
									Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(gold, resource);
									this.data.resources.put(position, newRes);
								}
							}

						}else {
							if(type.equals("Gold")) {
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, null);
								this.data.resources.put(position, newRes);
							}else{
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(null, resource);
								this.data.resources.put(position, newRes);
							}
						}
					}
				}
			}

		}
	}


	@Override
	public void action() {
		Ping();


		if (this.data.myMap==null){
			this.data.myMap= new MapRepresentation();
		}


		if (this.cpt_interlock_prio>5){
			this.data.priority=0;
			this.cpt_interlock_prio=0;
		}else{
			this.cpt_interlock_prio+=1;
		}


		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=null){
			MAJ_carte();

			if (this.data.interlocking==1){
				this.finished=1;
				return;
			}else{
				this.finished=0;
			}

			if (!this.data.myMap.hasOpenNode()){


				if (this.data.myMap.getClosedNodes().size()<25){

					if( !this.data.myMap.getAgentNodes().isEmpty() ) {
						/* Slight glitch where Agent is in a closed area and was blocked
						 * By allied agent without having time to share their map with a small com's reach radius
						 * --> Turn agent-node into open-node
						 */

						String nextAgentNode = this.data.myMap.getAgentNodes().get(0);
						this.data.myMap.addNode(nextAgentNode,MapAttribute.open);
						this.data.wumpusNodes.remove(nextAgentNode);
						finished=3;
						return;
					}

					/*
					 * Rare bug where agent is in a closed area and closed all its known nodes
					 * No fixed found but to set the nodes as open.
					 */
					List<String> closedNodes = this.data.myMap.getClosedNodes();
					for ( String node : closedNodes ){
						this.data.myMap.addNode(node,MapAttribute.open);
					}
					finished=3;
					return;
				}

				// Exploration DONE

				this.data.explored = 1;
				this.data.priority=0;
				this.data.nom_interlocuteur=null;
				this.data.interlocuteurNode=null;
				this.finished=2;
				return;
			}





			Attente();

			MaJ_contacts();

		}

	}


	public int onEnd(){
		return finished;
	}



}