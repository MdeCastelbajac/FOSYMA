package eu.su.mas.dedaleEtu.mas.agents.dummies.explo;

import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import dataStructures.tuple.Couple;

public class agent_data implements java.io.Serializable {

    public int interlocuteurpriority;
    public String interlocuteurGoalNode;
    public String GoalNode;
    public Hashtable<String, Integer> contacts;
    public Hashtable<String, Integer> contacts_taille_carte;
   
    // Resources HashMap 
    public Hashtable<String, Couple<Resource, Resource>> resources;
    // Date == total fsm executions + current; 
    // This is obviously not 100% accurate 
    // but should be robust to distributed systems
    public int date;
    // Wumpus Nodes
    public ArrayList<String> wumpusNodes;
    
    // leader agent only 
    public Hashtable<String, Couple<Integer, Integer>> capacities;
    // Store recent encountered agent for efficient com's
    public Set<String> recentlyEncountered;
    public String someone_on_rdv;
    
    public String nextNode;
    public String interlocuteurNode;
    public String nom_interlocuteur;
    public String previous_node_1;
    public String previous_node_2;
    public int interlocking;
    public int priority;
    public MapRepresentation myMap;
    public List<String> chemin_prio;
    public int chemin_prio_index;
	public int explored;
    public int stench;
    public Hashtable<String, Couple<String, List<String>>> feuilles_de_route;
    public int attente_reponse;
    public int onRdv;
    public boolean leaderR;
    public int leaderCpt;
    public int vitesse;
    public List<String> feuille_de_route;
    public String type;
    public int collected;
    public boolean collecting;
    public String rdv_point;
    public int laisse_passage;
    public List<String> envoi_des_receveurs;
    
    public agent_data(){
        this.nextNode=null;
        this.interlocuteurNode=null;
        this.GoalNode=null;
        this.someone_on_rdv=null;
        this.onRdv=0;
        this.feuilles_de_route=new Hashtable<String, Couple<String, List<String>>>();
        this.interlocking=0;
        this.envoi_des_receveurs=new ArrayList<String>();
        this.vitesse=1000;
        this.contacts=new Hashtable<String, Integer>();
        this.contacts_taille_carte=new Hashtable<String, Integer>();
        this.resources=new Hashtable<String, Couple<Resource,Resource>>();
        this.wumpusNodes=new ArrayList<String>();
        this.capacities=new Hashtable<String, Couple<Integer, Integer>>();
        this.leaderR=false;
        this.leaderCpt=0;
        this.recentlyEncountered=new HashSet<String>();
        this.nom_interlocuteur=null;
        this.nextNode=null;
        this.type=null;
        this.previous_node_1=null;
        this.previous_node_2=null;
        this.priority=0;
        this.myMap=null;
        this.interlocuteurGoalNode=null;
        this.chemin_prio=null;
        this.chemin_prio_index=0;
        this.explored=0;
        this.interlocuteurpriority=0;
        this.stench=0;
        this.attente_reponse=0;
        this.feuille_de_route=null;
        this.collected=0;
        this.collecting=false;
        this.rdv_point=null;
        this.laisse_passage=0;

    }

}
