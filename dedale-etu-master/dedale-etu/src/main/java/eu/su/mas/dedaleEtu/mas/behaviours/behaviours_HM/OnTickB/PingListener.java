package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.OnTickB;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.*;

/*
 * Conditionnaly sends pings
 * Checks Interlocking
 * Then checks the mailbox for all possible answers
 */
public class PingListener extends TickerBehaviour{
	private agent_data data;
	private List<String> list_agentNames;
	private AbstractDedaleAgent myagent;
	//private AbstractDedaleAgent myagent;
	/**
	 *
	 */
	private static final long serialVersionUID = -2058134622078521998L;


	private void MaJ_contact_sender(String agent_name){
		this.data.contacts.put(agent_name,0);
	}

	public PingListener(final Agent myagent,final List<String> agentNames, final agent_data data) {

		super(myagent, 1);
		this.data=data;
		this.list_agentNames=agentNames;
		this.myagent=(AbstractDedaleAgent) myagent;
		//super(myagent);
	}

	private void ask_for_maj(String tmp_rec_name){
		this.data.contacts.put(tmp_rec_name,0);

		ACLMessage msg_AskMaj = new ACLMessage(ACLMessage.INFORM);
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.setProtocol("AskForMaj");
		msg_AskMaj.setContent("   ");
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.addReceiver(new AID(tmp_rec_name, AID.ISLOCALNAME));
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskMaj);
	}

	private void message_non_interlock(ACLMessage msgReceived){

		if (msgReceived!=null) {


			String tmp_interlocuteur=msgReceived.getSender().getLocalName();
			String[] parsed_content =msgReceived.getContent().split(";");

			// debug wumpus confusion with ally agent
			this.data.myMap.addNode(parsed_content[1], MapAttribute.closed);
			Integer n = this.data.contacts.get(tmp_interlocuteur);

			// Not a recently encountered agent -> share-map

			int nb_noeuds_ouverts=Integer.parseInt( parsed_content[3]);
			int nb_noeuds_fermes=Integer.parseInt(parsed_content[4]);

			if (((n<0)||(n>30))||(nb_noeuds_ouverts==0)||(nb_noeuds_fermes> (this.data.myMap.getClosedNodes().size()+5))){
				ask_for_maj(msgReceived.getSender().getLocalName());
			}



			// Interlocking detection
			if (this.data.priority<Integer.parseInt(parsed_content[0])){
				ask_for_maj(msgReceived.getSender().getLocalName());

				this.data.interlocuteurpriority=Integer.parseInt(parsed_content[0]);
				this.data.nom_interlocuteur=tmp_interlocuteur;
				this.data.interlocuteurNode=parsed_content[1];
				this.data.interlocuteurGoalNode=parsed_content[2];
				this.data.priority=this.data.interlocuteurpriority-10;
				this.data.chemin_prio=null;
				this.data.interlocking=1;
			}

			this.data.contacts.put(msgReceived.getSender().getLocalName(),0);

		}
	}

	private void message_interlock(ACLMessage msgReceived){


		if (msgReceived!=null) {

			String tmp_interlocuteur = msgReceived.getSender().getLocalName();

			if (!tmp_interlocuteur.equals(this.myAgent.getLocalName())) {
				String nom_interlocuteur = msgReceived.getSender().getLocalName();
				String[] parsed_content = msgReceived.getContent().split(";");
				Integer n = this.data.contacts.get(nom_interlocuteur);
				int interlocuteurpriority = Integer.parseInt(parsed_content[0]);
				String interlocuteurNode = parsed_content[1];
				String interlocuteurGoalNode = parsed_content[2];

				int nb_noeuds_ouverts=Integer.parseInt( parsed_content[3]);
				int nb_noeuds_fermes=Integer.parseInt(parsed_content[4]);

				if (((n<0)||(n>30))||(nb_noeuds_ouverts==0)||(nb_noeuds_fermes> (this.data.myMap.getClosedNodes().size()+5))){
					ask_for_maj(msgReceived.getSender().getLocalName());
				}

				if (interlocuteurpriority > this.data.interlocuteurpriority) {
					ask_for_maj(nom_interlocuteur);

					this.data.nom_interlocuteur = nom_interlocuteur;
					this.data.interlocuteurpriority = interlocuteurpriority;
					this.data.interlocuteurNode = interlocuteurNode;
					this.data.interlocuteurGoalNode = interlocuteurGoalNode;
					this.data.chemin_prio = null;
					this.data.interlocking = 1;
					this.data.contacts.put(nom_interlocuteur, 0);

					try {
						this.data.chemin_prio = this.data.myMap.getShortestPath(this.myagent.getCurrentPosition(), this.data.interlocuteurGoalNode);
					} catch (Exception e) {
					}
				}

			}
		}
	}



	private void message_Wumpus(ACLMessage msgReceived){

		if (msgReceived!=null) {
			String nom_interlocuteur = msgReceived.getSender().getLocalName();

			Integer n = this.data.contacts.get(nom_interlocuteur);

			if (((n < 0) || (n > 5))) {
				this.data.contacts.put(nom_interlocuteur, 0);
				ask_for_maj(nom_interlocuteur);
			}

			ACLMessage msg_AskWumpus= new ACLMessage(ACLMessage.INFORM);
			msg_AskWumpus.setSender(this.myAgent.getAID());
			msg_AskWumpus.setProtocol("AskForWumpusANS");
			msg_AskWumpus.setContent(this.myagent.getCurrentPosition());
			msg_AskWumpus.addReceiver(new AID(nom_interlocuteur, AID.ISLOCALNAME));

			((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskWumpus);
		}
	}

	private void message_Someone_on_RDV(ACLMessage msgReceived){

		if (msgReceived!=null && this.data.onRdv==1) {

			String nom_interlocuteur = msgReceived.getSender().getLocalName();

			Integer n = this.data.contacts.get(nom_interlocuteur);
			if (!this.data.envoi_des_receveurs.contains(msgReceived.getSender().getLocalName())){
				this.data.envoi_des_receveurs.add(msgReceived.getSender().getLocalName());
			}


			ACLMessage msg_AskWumpus= new ACLMessage(ACLMessage.INFORM);
			msg_AskWumpus.setSender(this.myAgent.getAID());
			msg_AskWumpus.setProtocol("AskForSOMEONERDVANS");
			String ans=null;
			if (this.data.someone_on_rdv==null){
				ans = "nonjeuispasla";
			}else{
				ans = this.data.someone_on_rdv;
			}
			msg_AskWumpus.setContent(ans);
			msg_AskWumpus.addReceiver(new AID(nom_interlocuteur, AID.ISLOCALNAME));
			((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskWumpus);
		}
	}

	private void message_WumpusANS(ACLMessage msgReceived){

		if (msgReceived!=null) {
			String pos = msgReceived.getContent();
			this.data.myMap.addNode(pos,MapAttribute.closed);
			this.data.wumpusNodes.remove(pos);
		}
	}

	private void message_Someone_on_RDV_ANS(ACLMessage msgReceived){

		if (msgReceived!=null) {
			String pos = msgReceived.getContent();
			if (!pos.equals("nonjeuispasla")) {
				this.data.someone_on_rdv = pos;
			}
		}
	}

	@Override
	public void onTick() {

		MessageTemplate msg=MessageTemplate.and(
				MessageTemplate.MatchProtocol("Ping"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived = this.myAgent.receive(msg);


		if (this.data.interlocking==0){

			message_non_interlock(msgReceived);
		}else{

			message_interlock(msgReceived);
		}

		MessageTemplate msgW=MessageTemplate.and(
				MessageTemplate.MatchProtocol("AskForWumpus"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived_W = this.myAgent.receive(msgW);
		message_Wumpus(msgReceived_W);

		MessageTemplate msgWA=MessageTemplate.and(
				MessageTemplate.MatchProtocol("AskForWumpusANS"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived_WA = this.myAgent.receive(msgWA);

		MessageTemplate msgAsk=MessageTemplate.and(
				MessageTemplate.MatchProtocol("AskSomeoneOnRdv"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived_A = this.myAgent.receive(msgAsk);
		message_Someone_on_RDV(msgReceived_A);

		MessageTemplate msgAskANS=MessageTemplate.and(
				MessageTemplate.MatchProtocol("AskForSOMEONERDVANS"),
				MessageTemplate.MatchPerformative(ACLMessage.INFORM));
		ACLMessage msgReceived_ANS = this.myAgent.receive(msgAskANS);
		message_Someone_on_RDV_ANS(msgReceived_ANS);




		}
	}
