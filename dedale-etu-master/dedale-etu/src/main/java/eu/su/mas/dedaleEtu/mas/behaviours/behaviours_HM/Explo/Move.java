package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Explo;


import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.*;


/**
 * FSM MoveState
 *
 * Move to next node, 
 * Spots the wumpus  
 *
 *
 */
public class Move extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;



	private List<String> list_agentNames;
	private int finished;
	private agent_data data;
	private String ng;
	private AbstractDedaleAgent myagent;
	private int cpt;
	private int cpt1;

	public Move(final AbstractDedaleAgent myagent, List<String> agentNames,agent_data data) {
		super(myagent);
		this.list_agentNames=agentNames;
		this.finished=0;
		this.data=data;
		this.myagent=(AbstractDedaleAgent) myagent;
		ng=null;
		this.cpt=0;
		this.cpt1=0;

	}

	private void ask_for_wumpus(){
		ACLMessage msg_AskWumpus= new ACLMessage(ACLMessage.INFORM);
		msg_AskWumpus.setSender(this.myAgent.getAID());
		msg_AskWumpus.setProtocol("AskForWumpus");
		msg_AskWumpus.setContent("   ");
		msg_AskWumpus.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();

		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			int n = this.data.contacts.get(name);
			msg_AskWumpus.addReceiver(new AID(name, AID.ISLOCALNAME));

		}

		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskWumpus);



	}

	private void test_noeud_wumpus(String nextNode){
		ask_for_wumpus();
		this.data.wumpusNodes.add(nextNode);
		this.data.myMap.addNode(nextNode, MapAttribute.agent);
		find_next_node();

	}

	private void ask_for_maj(String tmp_rec_name){
		this.data.contacts.put(tmp_rec_name,0);

		ACLMessage msg_AskMaj = new ACLMessage(ACLMessage.INFORM);
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.setProtocol("AskForMaj");
		msg_AskMaj.setContent("   ");
		msg_AskMaj.setSender(this.myAgent.getAID());
		msg_AskMaj.addReceiver(new AID(tmp_rec_name, AID.ISLOCALNAME));
		((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskMaj);
	}

	private int find_next_node(){
		if (!this.data.myMap.hasOpenNode()){ return 0; }

		List <String> chemin =  this.data.myMap.getShortestPathToClosestOpenNode(this.myagent.getCurrentPosition());

		this.data.nextNode=chemin.get(0);
		this.ng=chemin.get(chemin.size()-1);
		return 1;
	}

	private void enregistrement_deplacement(){
		this.data.previous_node_2=this.data.previous_node_1;
		this.data.previous_node_1=this.myagent.getCurrentPosition();
	}
	@Override
	public void action() {

		this.data.date = this.data.date + 1;

		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();

		if (myPosition!=null){

			if (this.data.nextNode==null){

				if (find_next_node()==0){ return; }

			}

			enregistrement_deplacement();


			Boolean mov=((AbstractDedaleAgent)this.myAgent).moveTo(this.data.nextNode);

			if (!mov){
				cpt+=1;
			}else{
				cpt=0;
			}

			if (cpt>5){

				if (this.cpt1>5) {

					ACLMessage msg_AskMaj = new ACLMessage(ACLMessage.INFORM);
					msg_AskMaj.setSender(this.myAgent.getAID());
					msg_AskMaj.setProtocol("AskForMaj");
					msg_AskMaj.setContent("   ");
					msg_AskMaj.setSender(this.myAgent.getAID());


					for (String agent : this.list_agentNames) {
						msg_AskMaj.addReceiver(new AID(agent, AID.ISLOCALNAME));
					}

					((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskMaj);
					this.cpt1=0;
				}else{
					this.cpt1+=5;
				}

			}

			if((this.data.stench==1)&&(mov==false)) {
				test_noeud_wumpus(this.data.nextNode);
			}
			this.data.stench=0;
			this.data.nextNode=null;

		}
	}


	public int onEnd(){
		return finished;
	}



}