package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.reunion;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;


/*
 * Agent decides a gathering point and moves towards it.
 * It repeatedly checks if other agents are nearby
 */
public class VersPointRDV extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private int finished;
	private List<String> list_agentNames;
	private AbstractDedaleAgent myagent;
	private agent_data data;
	private String rdv_point;
	private int cpt;


	private int get_string_ID(String s){
		int tmp=1;

		for (int i=0; i<s.length();i++){
			if (i>0) {
				tmp *= (s.charAt(i) + s.charAt(i-1) + 1 );
			}else{
				tmp+= s.charAt(i);
			}
		}
		return tmp;

	}
	

	private String getLowestNameNode(List<String> nodes){
		String min_node=null;
		int min_node_tmp=-1;

		for (int i = 0; i < nodes.size(); i++) {
			if (min_node_tmp==-1||min_node_tmp>get_string_ID(nodes.get(i))){
				min_node= nodes.get(i) ;
				min_node_tmp=get_string_ID(nodes.get(i));
			}
		}

		return min_node;

	}

	/*
	private String get_biggest_node(List<String> nodes){
		String min_node=null;
		int min_node_tmp=-1;

		for (int i = 0; i < nodes.size(); i++) {
			if (min_node_tmp==-1||min_node_tmp<get_string_ID(nodes.get(i))){
				min_node= nodes.get(i) ;
				min_node_tmp=get_string_ID(nodes.get(i));
			}
		}

		return min_node;

	}*/



	private void ask_someone_on_rdv(){


		if (!this.rdv_point.equals(this.data.someone_on_rdv)){
			ACLMessage msg_AskMaj = new ACLMessage(ACLMessage.INFORM);
			msg_AskMaj.setSender(this.myAgent.getAID());
			msg_AskMaj.setProtocol("AskSomeoneOnRdv");
			msg_AskMaj.setContent("   ");
			msg_AskMaj.setSender(this.myAgent.getAID());

			Enumeration<String> dico_keys = this.data.contacts.keys();

			while (dico_keys.hasMoreElements()) {
				String name = dico_keys.nextElement();

				msg_AskMaj.addReceiver(new AID(name, AID.ISLOCALNAME));

			}

			((AbstractDedaleAgent) this.myAgent).sendMessage(msg_AskMaj);
		}
	}

	private void Ping(){
		System.out.println(this.myagent.getLocalName());
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		String myPosition= this.myagent.getCurrentPosition();
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("Ping");

		String tmp_goal_node = this.data.myMap.goalNode;

		if (this.data.previous_node_2 != null) {
			if ((this.data.previous_node_1.equals(this.data.previous_node_2)) && (this.data.previous_node_1.equals(this.myagent.getCurrentPosition()))) {
				Random rand = new Random();
				int tmp_prio=0;

				if (lobs.size() <= 2) {
					tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * 250 + 9999;
				} else {
					tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * 250;
				}

				if (tmp_prio>this.data.priority){
					this.data.priority=tmp_prio;
				}
			}
		}

		msg.setContent(this.data.priority + ";" + myPosition + ";" + tmp_goal_node);
		msg.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();

		int nb_destinataires = 0;

		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			int n = this.data.contacts.get(name);

			if ((n < 0) || (n > 10)) {
				msg.addReceiver(new AID(name, AID.ISLOCALNAME));
				nb_destinataires += 1;
			}
		}

		if (nb_destinataires >= 1) {
			((AbstractDedaleAgent) this.myAgent).sendMessage(msg);
		}
	}

	private void find_next_node(){
		List <String> chemin =  this.data.myMap.getShortestPath(this.myagent.getCurrentPosition(),this.rdv_point);
		this.data.nextNode=chemin.get(0);
	}


	public VersPointRDV(final AbstractDedaleAgent myagent, List<String> agentNames, agent_data data) {
		super(myagent);


		this.finished=0;
		this.cpt=0;
		this.list_agentNames=agentNames;
		this.myagent=myagent;
		this.data=data;
		this.rdv_point="-1";

	}

	@Override
	public void action() {
		

		if (this.data.myMap.hasOpenNode()){
			this.finished=2;
			return;
		}

		if (this.rdv_point.equals("-1")){
			this.data.nextNode=null;
			this.rdv_point = getLowestNameNode(this.data.myMap.getClosedNodes());
			System.out.println(this.myAgent.getLocalName()+" dit : Exploration terminéee. Je me dirige vers le noeud "+this.rdv_point+".");
			this.data.rdv_point=this.rdv_point;
			return;
		}
		
		if (!this.myagent.getCurrentPosition().equals(this.rdv_point)) {
			find_next_node();
		}else{
			this.data.someone_on_rdv=this.rdv_point;
			this.finished=1;
			this.data.onRdv=1;
			return;

		}


		this.data.previous_node_2=this.data.previous_node_1;
		this.data.previous_node_1=this.myagent.getCurrentPosition();
		Boolean mov=((AbstractDedaleAgent)this.myAgent).moveTo(this.data.nextNode);

		if ((this.myagent.getCurrentPosition().equals(this.rdv_point)||this.rdv_point.equals(this.data.someone_on_rdv))&& !mov ){
			this.data.someone_on_rdv=this.rdv_point;
			this.finished=1;
			this.data.onRdv=1;

			return;
		}

		if (!mov && (this.data.myMap.getShortestPath(this.myagent.getCurrentPosition(),this.rdv_point).size()<=(this.list_agentNames.size())*3)){
			int tmp_cpt=this.data.myMap.getShortestPath(this.myagent.getCurrentPosition(),this.rdv_point).size();
			if (this.cpt>tmp_cpt){
				ask_someone_on_rdv();
				this.cpt=0;
			}else{
				this.cpt+=1;
			}

		}





		try {
			this.myAgent.doWait(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}


	public int onEnd(){
		return finished;
	}



}
