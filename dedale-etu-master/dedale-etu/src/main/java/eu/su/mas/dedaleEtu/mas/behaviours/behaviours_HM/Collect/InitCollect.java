package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Collect;

import dataStructures.serializableGraph.SerializableSimpleGraph;
import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.ExploreCoopAgentHM;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


/**
 * FSM INIT STATE 
 *
 * Observe & updates knowledge
 * Chose next node to move to
 * Spots Interlocking
 * ! Collect Resources
 * 
 */

public class InitCollect extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;


	private int finished;
	private AbstractDedaleAgent myagent;
	private agent_data data;
	private int cpt_prio=0;
	private int cpt_ping;
	private int cpt_feuille_de_route;
	private int collecte_reussie;

	private int cpt_interlock_prio;


	public InitCollect(final AbstractDedaleAgent myagent, List<String> agentNames,agent_data data) {
		super(myagent);

		this.finished=0;
		this.cpt_feuille_de_route=0;
		this.myagent=myagent;
		this.data=data;
		this.cpt_interlock_prio=0;
		this.cpt_ping=0;
		this.collecte_reussie=0;
	}

	private void MaJ_contacts(){
		Enumeration<String> e = this.data.contacts.keys();

		while (e.hasMoreElements()) {
			String key = e.nextElement();
			Integer n = this.data.contacts.get(key);
			if (n>=0) {
				this.data.contacts.put(key, n + 1);
			}
		}
	}

	private void Attente(){
		try {
			this.myAgent.doWait(1000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

		private void MaJ_cpt_prio(){
			if (this.cpt_prio>5){
				this.data.priority=0;
				this.cpt_prio=0;
			}else{
				this.cpt_prio+=1;
			}
		}


	private void Ping(){
	
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

		String myPosition= this.myagent.getCurrentPosition();
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setSender(this.myAgent.getAID());
		msg.setProtocol("Ping");

		String tmp_goal_node = this.data.myMap.goalNode;


		if ((this.data.previous_node_1.equals(this.data.previous_node_2)) && (this.data.previous_node_1.equals(this.myagent.getCurrentPosition()))) {
			Random rand = new Random();
			int tmp_prio=0;

			if (lobs.size() <= 2) {
				tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * this.data.vitesse + 399999;
			} else {
				tmp_prio = rand.nextInt(150 + 1) + this.data.myMap.distanceNextNode * this.data.vitesse;
			}

			if (tmp_prio>this.data.priority){
				this.data.priority=tmp_prio;
				this.cpt_prio=0;
			}
		}


		msg.setContent(this.data.priority + ";" + myPosition + ";" + tmp_goal_node + ";" + this.data.myMap.getOpenNodes().size() + ";" + this.data.myMap.getClosedNodes().size());
		msg.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();

		int nb_destinataires = 0;

		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			int n = this.data.contacts.get(name);
			msg.addReceiver(new AID(name, AID.ISLOCALNAME));
			this.data.contacts.put(name,0);
		}


		((AbstractDedaleAgent) this.myAgent).sendMessage(msg);

	}




	private void MAJ_carte(){
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if(this.data.feuille_de_route.contains(myPosition)) {
			//
			//System.out.println(this.myAgent.getLocalName()+" arrivé a destination");
		}
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition

		this.data.nextNode=null;

		Iterator<Couple<String, List<Couple<Observation, Integer>>>> iter=lobs.iterator();
		
		while(iter.hasNext()){
			String nodeId=iter.next().getLeft();
			boolean isNewNode=false;
			if(( !this.data.myMap.getClosedNodes().contains(nodeId)&&(!this.data.myMap.getAgentNodes().contains(nodeId)))) {
				this.data.myMap.addNode(nodeId, MapAttribute.open);
				isNewNode=true;
			}
			
			// We chose close nodes to move to only if we don't know where to go by now
			if ((!Objects.equals(myPosition, nodeId))&&(this.data.feuille_de_route.isEmpty())) {
				this.data.myMap.addEdge(myPosition, nodeId);
				if (this.data.nextNode==null){ this.data.nextNode=nodeId;}
				Random rand = new Random();

				if (this.myagent.getLocalName().equals("A1")){
					int tmp =rand.nextInt(100);
					if (tmp>25){
						this.data.nextNode=nodeId;
						}
				}

				if (this.myagent.getLocalName().equals("A2")){
					int tmp =rand.nextInt(100);
					if (tmp>25){
						this.data.nextNode=nodeId;
					}
				}

				if (this.myagent.getLocalName().equals("A3")){
					int tmp =rand.nextInt(100);
					if (tmp>50){
						this.data.nextNode=nodeId;
					}
				}

				if (this.myagent.getLocalName().equals("A4")){
					int tmp =rand.nextInt(100);
					if (tmp>50){
						this.data.nextNode=nodeId;
					}
				}

				if (this.myagent.getLocalName().equals("A5")){
					int tmp =rand.nextInt(100);
					if (tmp>75){
						this.data.nextNode=nodeId;
					}
				}

				if (this.myagent.getLocalName().equals("A6")){
					int tmp =rand.nextInt(100);
					if (tmp>75){
						this.data.nextNode=nodeId;
					}
				}




			}
		}
		
		
		Couple<String,List<Couple<Observation, Integer>>> currentNodeObs=lobs.get(0);
		lobs.remove(0);
		for ( Couple<Observation, Integer> obs : currentNodeObs.getRight()) {
				if (obs.getLeft().getName().equals("Stench")) {
					this.data.stench = 1;
				}
				// COLLECT HERE
				//if (!this.data.feuille_de_route.isEmpty()) {
					//if (this.data.feuille_de_route.contains(myPosition)) {
						//System.out.println(this.myAgent.getLocalName()+" : "+((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace());
						int collected=0;
						switch (obs.getLeft()) {
						case DIAMOND:
							if(this.data.type.equals("Diamond")) {
								this.collecte_reussie=1;
								System.out.println(this.myAgent.getLocalName()+" : lockpic = "+((AbstractDedaleAgent) this.myAgent).openLock(obs.getLeft()));
								System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(1).getRight());
								collected = ((AbstractDedaleAgent) this.myAgent).pick();
								this.data.collected+=collected;
								System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+obs.getLeft() +": "+ obs.getRight());
								System.out.println(this.myAgent.getLocalName()+" - The agent grabbed :"+collected);
								System.out.println(this.myAgent.getLocalName()+" - the remaining backpack capacity is: "+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(1).getRight());
								this.data.feuille_de_route.remove(myPosition);
								if(((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(1).getRight()==0) {
									// Collect DONE
									finished=3;
								}
							}
						case GOLD:
							if(this.data.type.equals("Gold")) {
								this.collecte_reussie=1;
								System.out.println(this.myAgent.getLocalName()+" : lockpic = "+((AbstractDedaleAgent) this.myAgent).openLock(obs.getLeft()));
								System.out.println(this.myAgent.getLocalName()+" - My current backpack capacity is:"+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(0).getRight());
								System.out.println(this.myAgent.getLocalName()+" - Value of the treasure on the current position: "+obs.getLeft() +": "+ obs.getRight());
								collected = ((AbstractDedaleAgent) this.myAgent).pick();
								this.data.collected+=collected;
								System.out.println(this.myAgent.getLocalName()+" - The agent grabbed :"+collected);
								System.out.println(this.myAgent.getLocalName()+" - the remaining backpack capacity is: "+ ((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(0).getRight());
								this.data.feuille_de_route.remove(myPosition);
								if(((AbstractDedaleAgent) this.myAgent).getBackPackFreeSpace().get(0).getRight()==0) {
									// Collect DONE
									finished=3;
								}
							}
							
						}
					
				//}
			//}
		}
		
		this.data.myMap.addNode(this.myagent.getCurrentPosition(), MapAttribute.closed);
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs_2=((AbstractDedaleAgent)this.myAgent).observe();
		for( Couple<String, List<Couple<Observation, Integer>>> obs:lobs_2) {
			// Extracts current observations
			String position = obs.getLeft();
			List<Couple<Observation, Integer>> lo = obs.getRight();
			boolean remain=true;
			if (this.data.feuille_de_route.contains(position)) {
				remain=false;
			}
			if (!lo.isEmpty()) {
				for(Couple<Observation, Integer> o : lo) {
					String type = o.getLeft().getName();
					Integer quantity = o.getRight();
					// Updates data.resources
					if (type.equals("Gold")||type.equals("Diamond")) {
						remain=true;
						Resource resource = new Resource();
						resource.type = type;
						resource.quantity = quantity;
						resource.date = new Date();

						if (this.data.resources.containsKey(position)) {
							Couple<Resource, Resource> res = this.data.resources.get(position);
							if(type.equals("Gold")) {
								if (res.getLeft().date.before(resource.date)){
									Resource dia = res.getRight();
									this.data.resources.remove(position);
									Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, dia);
									this.data.resources.put(position, newRes);
								}
							}else {
								if (res.getRight().date.before(resource.date)){
									Resource gold = res.getLeft();
									this.data.resources.remove(position);
									Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(gold, resource);
									this.data.resources.put(position, newRes);
								}
							}
							
						}else {
							if(type.equals("Gold")) {
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(resource, null);
								this.data.resources.put(position, newRes);
							}else{
								Couple<Resource, Resource> newRes = new Couple<Resource, Resource>(null, resource);
								this.data.resources.put(position, newRes);
							}
						}
					}
				}
			}
			if (!remain) {
				this.data.feuille_de_route.remove(position);
				finished=2;
				return;
			}

		}
	}

	private void shareFeuillesDeRoute(){

		ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
		msg_3.setProtocol("SHARE_FEUILLES-de-ROUTE");
		msg_3.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();


		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			msg_3.addReceiver(new AID(name, AID.ISLOCALNAME));
		}

		try {
			msg_3.setContentObject(this.data.feuilles_de_route);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);

	}
	
	public boolean isFull() {
		if (this.data.type.equals("Gold")){
			return ( ((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace().get(0).getRight()==0 );
		}
		return ( ((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace().get(1).getRight()==0 );
				
	}

	private void gestion_strat_movement(){
		if ( !this.data.feuille_de_route.isEmpty() ) {
			this.data.collecting = true;
		}else {
			RandomMove();
			this.data.priority=0;
			finished=2;
		}
		if ( isFull() ) {
			RandomMove();
			this.data.priority=0;
			finished=2;
		}

	}

	@Override
	public void action() {

		if (this.collecte_reussie!=1) {
			shareFeuillesDeRoute();
			
		}

		if (((AbstractDedaleAgent)this.myAgent).observe().size()>=4 || this.cpt_ping>=3) {
			this.cpt_ping=0;
			Ping();
		}else{
			this.cpt_ping+=1;
		}
		MaJ_cpt_prio();
		gestion_strat_movement();
		

		MAJ_carte();
		MaJ_contacts();
		Attente();

		//0) Retrieve the current position
		String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		if (this.data.feuille_de_route!=null) {
			if (!this.data.feuille_de_route.isEmpty()) {
				if( this.data.previous_node_1.equals(this.data.previous_node_2) && (this.data.previous_node_1.equals(myPosition))) {
					this.data.feuille_de_route.remove(0);
					finished=0;
					return;
				}
			}
		}
		if (myPosition!=null){
			if (this.data.interlocking==1){
				this.data.laisse_passage=0;
				this.finished=1;
				return;
			}else{
				this.finished=0;
			}
		}
			
		if ( this.data.feuille_de_route.isEmpty() ) {
			this.data.collecting = false;
		}
		if (isFull () ) {
			//System.out.println(this.myAgent.getLocalName()+" a collecté "+this.data.collected+" "+this.data.type);

		}
			
	}


	private void RandomMove() {
		//String myPosition=((AbstractDedaleAgent)this.myAgent).getCurrentPosition();
		List<Couple<String,List<Couple<Observation,Integer>>>> lobs=((AbstractDedaleAgent)this.myAgent).observe();//myPosition
		//Random move from the current position
		Random r= new Random();
		int moveId=1+r.nextInt(lobs.size()-1);//
		((AbstractDedaleAgent)this.myAgent).moveTo(lobs.get(moveId).getLeft());

	}

	public int onEnd(){
		return finished;
	}



}
