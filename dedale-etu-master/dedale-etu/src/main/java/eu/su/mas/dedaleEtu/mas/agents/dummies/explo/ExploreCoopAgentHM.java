package eu.su.mas.dedaleEtu.mas.agents.dummies.explo;

import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedale.mas.agent.behaviours.startMyBehaviours;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Collect.InitCollect;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Collect.InterlockingCollect;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Collect.MoveCollect;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.Explo.*;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.reunion.*;
import eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.OnTickB.*;


import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import jade.core.AID;
import jade.core.behaviours.Behaviour;

import jade.core.Agent;
import jade.core.behaviours.DataStore;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.FSMBehaviour;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

/**
 * <pre>
 * ExploreCoop agent.
 * Basic example of how to "collaboratively" explore the map
 *  - It explore the map using a DFS algorithm and blindly tries to share the topology with the agents within reach.
 *  - The shortestPath computation is not optimized
 *  - Agents do not coordinate themselves on the node(s) to visit, thus progressively creating a single file. It's bad.
 *  - The agent sends all its map, periodically, forever. Its bad x3.
 *   - You should give him the list of agents'name to send its map to in parameter when creating the agent.
 *   Object [] entityParameters={"Name1","Name2};
 *   ag=createNewDedaleAgent(c, agentName, ExploreCoopAgent.class.getName(), entityParameters);
 *
 * It stops when all nodes have been visited.
 *
 *
 *  </pre>
 *
 * @author hc
 *
 */


public class ExploreCoopAgentHM extends AbstractDedaleAgent {

	private static final long serialVersionUID = -7969469610241668140L;
	private MapRepresentation myMap;
	private List<String> list_agentNames;
	private agent_data data;





	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time.
	 * 			1) set the agent attributes
	 *	 		2) add the behaviours
	 *
	 */

	protected void setup(){


		super.setup();
		//get the parameters added to the agent at creation (if any)
		final Object[] args = getArguments();

		List<String> list_agentNames=new ArrayList<String>();
		this.data= new agent_data();
		if(args.length==0){
			System.err.println("Error while creating the agent, names of agent to contact expected");
			System.exit(-1);
		}else{
			int i=2;// WARNING YOU SHOULD ALWAYS START AT 2. This will be corrected in the next release.
			while (i<args.length) {
				String tmp_name = (String)args[i];
				if (!tmp_name.equals(this.getLocalName())){
					list_agentNames.add(tmp_name);
					System.out.println(this.getLocalName()+"   "+tmp_name);
				}

				i++;
			}
		}

		Iterator<String> iter=list_agentNames.iterator();
		while(iter.hasNext()) {
			String agent = iter.next();
			this.data.contacts.put(agent, -1);
			this.data.contacts_taille_carte.put(agent, -1);
		}



		/******
		 * Initialisation de l automate
		 *******/

		FSMBehaviour fsm = new FSMBehaviour(this) {
			public int onEnd() {
				System.out.println("FSM behaviour terminé");
				myAgent.doDelete();
				return super.onEnd();
			}

		};

		List<Behaviour> lb=new ArrayList<Behaviour>();
		lb.add(fsm);

		// INIT STATE
		fsm.registerFirstState (new Init(this,list_agentNames,this.data),"Init");
		// MOVE STATE
		fsm.registerState(new Move(this,list_agentNames,this.data),"Move");
		// INTERLOCKING STATE
		fsm.registerState(new Interlocking_s(this,list_agentNames,this.data),"Interlocking_s");
		// TOWARDS GATHERING STATE
		fsm.registerState(new VersPointRDV(this,list_agentNames,this.data),"VersPointRDV");
		// GATHERING STATE
		fsm.registerState(new Gathering(this,list_agentNames,this.data),"G");
		// GATHERING LEADER STATE
		fsm.registerState(new GatheringLeader(this,list_agentNames,this.data),"G1");
		// GATHERING NON LEADER STATE
		fsm.registerState(new GatheringWaiter(this,list_agentNames,this.data),"G2");
		// DONE STATE
		fsm.registerLastState(new Done(),"Done");
		fsm.registerState(new InitCollect(this,list_agentNames,this.data),"InitC");
		fsm.registerState(new MoveCollect(this,list_agentNames,this.data),"MoveC");
		fsm.registerState(new InterlockingCollect(this,list_agentNames,this.data),"InterC");

		// Standard transition
		fsm.registerTransition("Init","Move",0);
		fsm.registerTransition("Move","Init",0);
		fsm.registerTransition("Init", "Init", 3);
		// Interlocking Detection and Solving
		fsm.registerTransition("Init", "Interlocking_s", 1);
		fsm.registerTransition("Interlocking_s","Interlocking_s",0);
		fsm.registerTransition("Interlocking_s","Init",1);


		// On Exploration Termination
		// Towards Gathering
		fsm.registerTransition("Init","VersPointRDV",2);
		fsm.registerTransition("VersPointRDV","VersPointRDV",0);
		fsm.registerTransition("VersPointRDV","G",1);
		fsm.registerTransition("VersPointRDV","Init",2);
		// Gathering
		fsm.registerTransition("G","G",0);
		fsm.registerTransition("G","G1",1);
		fsm.registerTransition("G","G2",2);
		fsm.registerTransition("G1","G1",0);
		fsm.registerTransition("G2","G2",0);

		fsm.registerTransition("G1","InitC",1);
		fsm.registerTransition("G2","InitC",1);
		fsm.registerTransition("InitC","MoveC",0);
		fsm.registerTransition("MoveC","InitC",0);
		fsm.registerTransition("InitC","InterC",1);
		fsm.registerTransition("InterC","InitC",1);
		fsm.registerTransition("InterC","InterC",0);
		fsm.registerTransition("InitC","InitC",2);
		/***
		 * MANDATORY TO ALLOW YOUR AGENT TO BE DEPLOYED CORRECTLY
		 */

		// Standalone Com's Behaviours
		addBehaviour(new MergeKnowledge(this, data));
		addBehaviour(new PingListener(this,list_agentNames, data));
		addBehaviour(new startMyBehaviours(this,lb));

		System.out.println("the  agent "+this.getLocalName()+ " is started");



	}





}