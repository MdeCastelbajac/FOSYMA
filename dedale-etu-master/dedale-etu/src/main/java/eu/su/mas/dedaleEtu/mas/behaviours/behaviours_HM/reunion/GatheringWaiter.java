package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.reunion;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation;
import eu.su.mas.dedaleEtu.mas.knowledge.MapRepresentation.MapAttribute;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.IOException;
import java.util.*;

import static java.lang.Math.*;


/**
 * The Agent is not the leader 
 * It sends its own data then waits for a decision
 */
public class GatheringWaiter extends OneShotBehaviour{

	private static final long serialVersionUID = 8567689731496787661L;

	private int finished;
	private List<String> list_agentNames;
	private AbstractDedaleAgent myagent;
	private agent_data data;
	private String rdv_point;
	private boolean answered;
	private int cpt;
	private int attente;
	private int mode;
	private int tmp_avancement_phase1;
	private int cap_gold;
	private int cap_diam;

	public GatheringWaiter(final AbstractDedaleAgent myagent, List<String> agentNames, agent_data data) {
		super(myagent);

		this.finished=0;
		this.list_agentNames=agentNames;
		this.myagent=myagent;
		this.data=data;
		this.cap_diam=0;
		this.cap_gold=0;
		answered=false;
		this.cpt=0;
		this.attente=-1;
		this.mode=-1;
		this.tmp_avancement_phase1=-1;
	}


	private void shareBackpack(){

		ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
		msg_3.setProtocol("SHARE_BACKPACK-C");
		msg_3.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();



		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			msg_3.addReceiver(new AID(name, AID.ISLOCALNAME));
		}

		Hashtable<String, Couple<Integer, Integer>> sl=this.data.capacities;
		try {
			msg_3.setContentObject(sl);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);
	}

	public void answer(ACLMessage req, String leader) {
		List<Couple<Observation, Integer>> myBP = ((AbstractDedaleAgent)this.myAgent).getBackPackFreeSpace();
		int myGoldCapacity = myBP.get(0).getRight();
		int myDiamondCapacity = myBP.get(1).getRight();
		ACLMessage answer = new ACLMessage(ACLMessage.INFORM);
		answer.setProtocol("BP-ANSWER");
		answer.setSender(this.myAgent.getAID());
		answer.addReceiver(new AID(leader, AID.ISLOCALNAME));

		Couple<Integer,Integer> data=new Couple<Integer, Integer>(myGoldCapacity, myDiamondCapacity);
		try {
			answer.setContentObject(data);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(answer);
		System.out.println(this.myAgent.getLocalName()+" : answered");

	}

	private void phase_init(){
		this.attente= (this.list_agentNames.size())*3      -  this.data.myMap.getShortestPath(this.myagent.getCurrentPosition(),this.data.someone_on_rdv).size();

		Couple<Integer, Integer> newC = new Couple<Integer,Integer> ( (  ((AbstractDedaleAgent)this.myagent).getBackPackFreeSpace().get(0).getRight()), ((AbstractDedaleAgent)this.myagent).getBackPackFreeSpace().get(1).getRight())  ;
		this.data.capacities.put(this.myagent.getLocalName(),newC);
		this.mode=0;
	}

	private void phase_1(){
		if ((this.cpt>this.attente)||(this.tmp_avancement_phase1<this.data.capacities.size())){
			this.cpt=0;
			this.tmp_avancement_phase1=this.data.capacities.size();

			shareBackpack();
		}else{
			this.cpt+=1;
		}



		if ((this.data.capacities.keySet().size()  )>=this.list_agentNames.size()+1){
			this.attente=1;
			shareBackpack();
			this.mode=1;
		}
	}



	private void choisir_vitesse(){
		int cap_gold_perso=this.data.capacities.get(this.myagent.getLocalName()).getLeft();
		//int cap_diam_perso=this.myagent.getBackPackFreeSpace().get(1).getRight();


		int cpt=cap_gold_perso;
		for (String agent : this.list_agentNames){
			cpt+=this.data.capacities.get(agent).getLeft();
		}

		double moyenne= cpt/(this.list_agentNames.size()*1.0);
		double ecart = (cap_gold_perso-moyenne)/(moyenne*1.0);



		if (ecart>0){

			this.data.vitesse=max((int) (1000.0*(1-ecart)),700);
		}else{

			this.data.vitesse=min((int) (1000.0*(-1*ecart+1)),1250);
		}

	}

	private void phase_2(){

		this.cap_gold=0;
		this.cap_diam=0;
		int nb_coffres_gold=0;
		int nb_coffres_diamants=0;

		for ( String name : this.data.resources.keySet()) {
			Couple<Resource, Resource> r = this.data.resources.get(name);
			if (r.getLeft() != null) {
				this.cap_gold += r.getLeft().quantity;
				nb_coffres_gold += 1;
			}
			if (r.getRight() != null) {
				this.cap_diam += r.getRight().quantity;
				nb_coffres_diamants += 1;
			}


		}


	}


	public void action() {

		if (this.mode==-1){
			phase_init();
		}



		this.myagent.doWait(500);

		if (this.mode==0){
			phase_1();
		}

		if(this.mode==1){
			phase_2();
			choisir_vitesse();
			this.mode=2;
		}

		if (this.mode==2){
			if (this.data.feuille_de_route!=null){
				this.mode=4;
				this.cpt=0;
			}

		}

		if (this.mode==4){
			this.cpt+=1;
		}

		if (this.mode==4 && (this.data.interlocking==0 )){
			System.out.println(this.myagent.getLocalName()+"   je procede a la collecte");
			this.mode=5;
			finished=1;
			this.cpt=0;
		}

		if (this.mode==5){

			if (this.cpt>25){
				this.mode=6;
			}else{
				this.cpt+=1;
			}
		}

		if (this.mode==6){
			finished=1;
		}


	}


	public int onEnd(){
		return finished;
	}



}