package eu.su.mas.dedaleEtu.mas.knowledge;

import java.io.Serializable;
import java.util.Date;

import eu.su.mas.dedale.env.Observation;

/*
 * Tuple Structure to store Resources on the map
 */
public class Resource implements Serializable{
	public String type;
	public Integer quantity;
	public Date date;
	
	public Resource() {
		this.type = null;
		this.quantity = 0;
		this.date=null;
	}
}
