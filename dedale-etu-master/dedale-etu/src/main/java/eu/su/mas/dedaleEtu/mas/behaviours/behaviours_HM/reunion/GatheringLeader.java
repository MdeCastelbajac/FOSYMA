package eu.su.mas.dedaleEtu.mas.behaviours.behaviours_HM.reunion;

import dataStructures.tuple.Couple;
import eu.su.mas.dedale.env.Observation;
import eu.su.mas.dedale.mas.AbstractDedaleAgent;
import eu.su.mas.dedaleEtu.mas.agents.dummies.explo.agent_data;
import eu.su.mas.dedaleEtu.mas.knowledge.Resource;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;

import static java.lang.Math.*;


/*
 * The Agent is the leader,
 * It waits for agents' data
 * then compute and send a decision
 */


public class GatheringLeader extends OneShotBehaviour {

	private static final long serialVersionUID = 8567689731496787661L;

	private int finished;
	private List<String> list_agentNames;
	private List<String> list_agentNames_sort;
	private AbstractDedaleAgent myagent;
	private agent_data data;
	private Hashtable<String, Couple<Integer, Integer>> capacities;
	private int attente;
	private Hashtable<String, Couple<String,String>> res;
	private int cap_gold;
	private int cap_diam;

	private Hashtable<String, Couple<Integer, Integer>> resources;
	private Hashtable<String,String> res_naif;
	private Hashtable<String,Integer> res_vitesse;
	private Hashtable<String,String> res_type;
	private Hashtable<String, Couple<String, List<String>>> bestSheet;

	public GatheringLeader(final AbstractDedaleAgent myagent, List<String> agentNames, agent_data data) {
		super(myagent);

		this.finished=0;
		this.list_agentNames=agentNames;
		this.myagent=myagent;
		this.data=data;
		this.attente=-1;
		this.list_agentNames_sort = new ArrayList<>();
		this.cap_gold=0;
		this.cap_diam=0;
		this.res_naif=new Hashtable<String, String>();
		this.res_vitesse=new Hashtable<String, Integer>();
		this.res_type=new Hashtable<String, String>();



		this.res=new Hashtable<String, Couple<String,String>>();
		this.bestSheet = new Hashtable<String, Couple<String, List<String>>>();
		}



// Plan de la réunion :
	/*
	 *  L'agent 1 est chef de la réunion,
	 *  il request les sac à dos de tout le monde
	 *  il enregistre les capacites dans une liste de couples (taille == nb agents)
	 *
	 *  ensuite capacite_tot, ressources_tot, ratio etc.
	 *
	 *
	 *  les autres agents vérifient leurs boites au lettres régulièrement
	 *
	 *
	 */


	private void shareFeuillesDeRoute(){
		
		ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
		msg_3.setProtocol("SHARE_FEUILLES-de-ROUTE");
		msg_3.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();


		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			msg_3.addReceiver(new AID(name, AID.ISLOCALNAME));
		}

		try {
			msg_3.setContentObject(this.bestSheet);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);

	}

	private void shareBackpack(){

		ACLMessage msg_3 = new ACLMessage(ACLMessage.INFORM);
		msg_3.setProtocol("SHARE_BACKPACK-C");
		msg_3.setSender(this.myAgent.getAID());

		Enumeration<String> dico_keys = this.data.contacts.keys();



		while (dico_keys.hasMoreElements()) {
			String name = dico_keys.nextElement();
			msg_3.addReceiver(new AID(name, AID.ISLOCALNAME));
		}

		Hashtable<String, Couple<Integer, Integer>> sl=this.data.capacities;
		try {
			msg_3.setContentObject(sl);
		} catch (IOException e) {
			e.printStackTrace();
		}
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg_3);
	}

	private void phase_init(){
		Couple<Integer, Integer> newC = new Couple<Integer,Integer> ( (  ((AbstractDedaleAgent)this.myagent).getBackPackFreeSpace().get(0).getRight()), ((AbstractDedaleAgent)this.myagent).getBackPackFreeSpace().get(1).getRight())  ;
		this.data.capacities.put(this.myagent.getLocalName(),newC);
		this.attente=0;
	}

	private void phase_1(){
		this.attente=1;
		System.out.print(this.myagent.getLocalName()+"  les backpack de l equipe sont : ");
		shareBackpack();
		System.out.println(this.data.capacities);
	}

/*
	private void phase_2(){
		int collectables_diamants=0;
		int collectables_golds=0;
		int nb_coffres_gold=0;
		int nb_coffres_diamants=0;
		for ( String name : this.data.resources.keySet()) {
			Couple<Resource, Resource> r = this.data.resources.get(name);
			if (r.getLeft() != null) {
				collectables_golds += r.getLeft().quantity;
				nb_coffres_gold += 1;
			}
			if (r.getRight() != null) {
				collectables_diamants += r.getRight().quantity;
				nb_coffres_diamants += 1;
			}

			Hashtable<String, Couple<Integer,Integer>> tmp_resources= new Hashtable<String, Couple<Integer,Integer>>();
			int gold = 0;
			int diamond = 0;
			for (String r_name : this.data.resources.keySet()){
				gold = 0;
				diamond = 0;
				if( this.data.resources.get(r_name).getLeft()!= null )gold=this.data.resources.get(r_name).getLeft().quantity;
				if( this.data.resources.get(r_name).getRight()!= null )diamond=this.data.resources.get(r_name).getRight().quantity;
				tmp_resources.put(r_name, new Couple<Integer, Integer>(gold, diamond));
			}
			this.resources = tmp_resources;
			if( collectables_golds >= collectables_diamants) {
				sortGold(tmp_resources);
			}
			else {
				sortDiamonds(tmp_resources);
			}
		}
	}
*/


	private void phase_2(){

		this.cap_gold=0;
		this.cap_diam=0;
		int nb_coffres_gold=0;
		int nb_coffres_diamants=0;

		for ( String name : this.data.resources.keySet()) {
			Couple<Resource, Resource> r = this.data.resources.get(name);
			if (r.getLeft() != null) {
				this.cap_gold += r.getLeft().quantity;
				nb_coffres_gold += 1;
			}
			if (r.getRight() != null) {
				this.cap_diam += r.getRight().quantity;
				nb_coffres_diamants += 1;
			}


		}

		choisir_vitesse(this.myagent.getLocalName());
		for (String agent_name : this.list_agentNames){
			choisir_vitesse(agent_name);
		}
	}

	private void sortDiamonds(Hashtable<String, Couple<Integer, Integer>> ressources) {
		// TODO Auto-generated method stub
		// Sort resources by value
		TreeMap<Integer, String> tm_res= new TreeMap<Integer, String>();
		for( String pos: ressources.keySet()) {
			tm_res.put(ressources.get(pos).getRight(), pos);
		}
		// Get a set of the entries
		Set set_res = tm_res.entrySet();
		// Sort Agents by BP_Capacity
		TreeMap<Integer, String> tm_cap= new TreeMap<Integer, String>();
		for( String pos: this.data.capacities.keySet()) {
			tm_cap.put(this.data.capacities.get(pos).getRight(), pos);
		}
		// Get a set of the entries
		Set set_cap = tm_cap.entrySet();

		decideDiamond(set_cap, set_res);
	}



	private void decideDiamond(Set set_cap, Set set_res) {
		// TODO Auto-generated method stub
		// We assign tiny treasures to tiny backpacks first to maintain equity
		ArrayList<String> list_cap=new ArrayList<String>(set_cap);
		ArrayList<String> list_res=new ArrayList<String>(set_res);
		for( String name: list_cap) {
			int capacity = this.data.capacities.get(name).getRight();
			for( String pos: list_res) {
				int quantity = this.resources.get(pos).getRight();
				if (quantity==0) {
					continue;
				}
				if( list_res.isEmpty() ) {
					this.res.put(name,new Couple<String,String>("Gold","-1"));
					continue;
				}

				this.res.put(name,new Couple<String,String>("Gold",list_res.get(0)));
				list_res.remove(0);
			}
		}
	}



	private void sortGold(Hashtable<String, Couple<Integer, Integer>> ressources) {
		// TODO Auto-generated method stub
		// Sort Resources by value
		TreeMap<Integer, String> tm_res= new TreeMap<Integer, String>();
		for( String pos: ressources.keySet()) {
			tm_res.put(ressources.get(pos).getLeft(), pos);
		}
		// Get a set of the entries
		Set set_res = tm_res.entrySet();

		// Sort Agents by BP_Capacity
		TreeMap<Integer, String> tm_cap= new TreeMap<Integer, String>();
		for( String pos: this.data.capacities.keySet()) {
			tm_cap.put(this.data.capacities.get(pos).getLeft(), pos);
		}
		// Get a set of the entries
		Set set_cap = tm_cap.entrySet();
		decideGold( set_cap, set_res );
	}



	private void choisir_vitesse(String agent_name){
		int cap_gold_perso=this.data.capacities.get(agent_name).getLeft();
		//int cap_diam_perso=this.myagent.getBackPackFreeSpace().get(1).getRight();


		int cpt=this.data.capacities.get(this.myagent.getLocalName()).getLeft();

		for (String agent : this.list_agentNames){
			cpt+=this.data.capacities.get(agent).getLeft();
		}

		double moyenne= cpt/(this.list_agentNames.size()*1.0);
		double ecart = (cap_gold_perso-moyenne)/(moyenne*1.0);
		Integer vitesse = 1000;



		/*if (ecart>0){

			vitesse=max((int) (1000.0*(1-ecart)),700);
		}else{

			vitesse=min((int) (1000.0*(-1*ecart+1)),1250);
		}
*/
		this.res_vitesse.put(agent_name,vitesse);



	}

	private void decideGold(Set<String> set_cap, Set<String> set_res) {

		// We assign tiny treasures to tiny backpacks first to maintain equity

		ArrayList<String> list_cap=new ArrayList<String>(set_cap);

		ArrayList<String> list_res=new ArrayList<String>(set_res);
		for( String name: list_cap) {
			int capacity = this.data.capacities.get(name).getLeft();
			for( String pos: list_res) {
				int quantity = this.resources.get(pos).getLeft();
				if (quantity==0) {
					continue;
				}
				if( list_res.isEmpty() ) {
					this.res.put(name,new Couple<String,String>("Diamond","-1"));
					continue;
				}

				this.res.put(name,new Couple<String,String>("Diamond",list_res.get(0)));
				list_res.remove(0);
			}
		}
	}





	private void sendCollectOrder(String name, String pos) {
		ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
		msg.setProtocol("ORDER");
		msg.setSender(this.myAgent.getAID());
		msg.addReceiver(new AID(name, AID.ISLOCALNAME));
		msg.setContent(pos);
		((AbstractDedaleAgent)this.myAgent).sendMessage(msg);
	}

	private void getmin_agent_cap_gold(List <String> already_selected_agents ){
		int min_cap=-1;
		String min_name=null;

		for (String agent_name : this.data.capacities.keySet()){
			//int tmp_cap = min(this.data.capacities.get(agent_name).getLeft(),this.data.capacities.get(agent_name).getRight());
			int tmp_cap = this.data.capacities.get(agent_name).getLeft();
			if ( (min_cap==-1 || tmp_cap<min_cap)&&(!already_selected_agents.contains(agent_name))){
				min_cap=tmp_cap;
				min_name=agent_name;


			}
		}

		if (already_selected_agents.size()>0){
			already_selected_agents.add(already_selected_agents.size(),min_name);
		}else{
			already_selected_agents.add(min_name);
		}

	}

	private void getmin_agent_cap_diam(List <String> already_selected_agents ){
		int min_cap=-1;
		String min_name=null;

		for (String agent_name : this.data.capacities.keySet()){
			//int tmp_cap = min(this.data.capacities.get(agent_name).getLeft(),this.data.capacities.get(agent_name).getRight());
			int tmp_cap = this.data.capacities.get(agent_name).getRight();
			if ( (min_cap==-1 || tmp_cap<min_cap)&&(!already_selected_agents.contains(agent_name))){
				min_cap=tmp_cap;
				min_name=agent_name;


			}
		}

		if (already_selected_agents.size()>0){
			already_selected_agents.add(already_selected_agents.size(),min_name);
		}else{
			already_selected_agents.add(min_name);
		}

	}
	private void getmin_tresor_cap_gold(List <String> already_selected_agents ){
		int min_cap=-1;
		String min_name=null;

		for (String ressource_r : this.data.resources.keySet()){
			Resource gold = this.data.resources.get(ressource_r).getLeft();
			if( gold != null) {
				int tmp_cap= gold.quantity;
				if ( (min_cap==-1 || tmp_cap<min_cap)&&(!already_selected_agents.contains(ressource_r))){
					min_cap=tmp_cap;
					min_name=ressource_r;
				}
			}
			/*Resource diam = this.data.resources.get(ressource_r).getRight();
			if( diam != null) {
				this.data.resources.remove(ressource_r);
			}*/
		}

		if (already_selected_agents.size()>0){
			already_selected_agents.add(already_selected_agents.size(),min_name);
		}else{
			already_selected_agents.add(min_name);
		}

	}

	private void getmin_tresor_cap_diam(List <String> already_selected_agents ){
		int min_cap=-1;
		String min_name=null;

		for (String ressource_r : this.data.resources.keySet()){
			Resource gold = this.data.resources.get(ressource_r).getRight();
			if( gold != null) {
				int tmp_cap= gold.quantity;
				if ( (min_cap==-1 || tmp_cap<min_cap)&&(!already_selected_agents.contains(ressource_r))){
					min_cap=tmp_cap;
					min_name=ressource_r;
				}
			}
			/*Resource diam = this.data.resources.get(ressource_r).getRight();
			if( diam != null) {
				this.data.resources.remove(ressource_r);
			}*/
		}

		if (already_selected_agents.size()>0){
			already_selected_agents.add(already_selected_agents.size(),min_name);
		}else{
			already_selected_agents.add(min_name);
		}

	}
	private List<String> reverse_c(List <String> L){
		List <String> tmp = new ArrayList<>();

		for (String  s : L){
			tmp.add(0,s);
		}

		return tmp;
	}

	private void choisir_type(){
		List <String> agents_sacs_gold = new ArrayList<>();
		List <String> agents_sacs_diam = new ArrayList<>();
		List <String> ressources_map_gold = new ArrayList<>();
		List <String> ressources_map_diam = new ArrayList<>();
		Hashtable <String, Couple<Integer,Integer >> tmp_cap_gold = new Hashtable<String, Couple<Integer,Integer >>();
		Hashtable <String, Couple<Integer,Integer >> tmp_cap_diam = new Hashtable<String, Couple<Integer,Integer >>();

		Hashtable<String,Integer> scores_gold = new  Hashtable<String,Integer>();
		Hashtable<String,Integer> scores_diam = new  Hashtable<String,Integer>();

		for (String agent : this.data.capacities.keySet()){
			Couple<Integer,Integer> tmp_couple = new Couple<Integer,Integer>(this.data.capacities.get(agent).getLeft(),this.data.capacities.get(agent).getRight());
			tmp_cap_gold.put(agent,tmp_couple);
			scores_gold.put(agent,0);
		}
		for (String agent : this.data.capacities.keySet()){
			Couple<Integer,Integer> tmp_couple = new Couple<Integer,Integer>(this.data.capacities.get(agent).getRight(),this.data.capacities.get(agent).getRight());
			tmp_cap_diam.put(agent,tmp_couple);
			scores_diam.put(agent,0);
		}
		
		for( String pos : this.data.resources.keySet()) {
			Resource res=this.data.resources.get(pos).getLeft();
			if ( (res != null)&&(pos!=null) ){
				ressources_map_gold.add(pos);
			}
		}
		for( String pos : this.data.resources.keySet()) {
			Resource res=this.data.resources.get(pos).getRight();
			if ( (res != null)&&(pos!=null) ){
				ressources_map_diam.add(pos);
			}	
		}
		
		
		
		for (int i=0;i<this.data.capacities.size();i++){
			getmin_agent_cap_gold(agents_sacs_gold);
		}
		
		for (int i=0;i<this.data.capacities.size();i++){
			getmin_agent_cap_diam(agents_sacs_diam);
		}


		for (int i=0;i<this.data.resources.size();i++){
			getmin_tresor_cap_gold(ressources_map_gold);
		}
		for (int i=0;i<this.data.resources.size();i++){
			getmin_tresor_cap_diam(ressources_map_diam);
		}
		
		
		List <String> ressources_map_gold_reversed= reverse_c(ressources_map_gold);
		List <String> ressources_map_diam_reversed= reverse_c(ressources_map_diam);
		
		// DIAMOND AGENTS
		String bestdiam1 = agents_sacs_diam.get(agents_sacs_diam.size()-1);
		String bestdiam2 = agents_sacs_diam.get(agents_sacs_diam.size()-2);
		
		List<String> ok_1 = new ArrayList<String>();
		List<String> ok_2 = new ArrayList<String>();
		for(String s: ressources_map_diam) {
			if(s!=null) {
				ok_1.add(s);
			}
		}
		for(String s: ressources_map_diam_reversed) {
			if(s!=null) {
				ok_2.add(s);
			}
		}
		Couple<String, List<String>>tmp_couple=new Couple<String, List<String>>("Diamond", ok_1);
		Couple<String, List<String>>tmp_couple_2=new Couple<String, List<String>>("Diamond", ok_2);
		this.bestSheet.put(bestdiam1, tmp_couple_2);
		this.bestSheet.put(bestdiam2, tmp_couple);
		
		agents_sacs_gold.remove(bestdiam1);
		agents_sacs_gold.remove(bestdiam2);
		
		Hashtable<String, Couple<Integer,Integer>> tmp_tresor_quantities_gold=new Hashtable<String, Couple<Integer,Integer>>();
		for (String ressource_r : this.data.resources.keySet()) {
			int gold_amount=0;
			int diam_amount=0;
			if (this.data.resources.get(ressource_r).getLeft()!=null){
				gold_amount=this.data.resources.get(ressource_r).getLeft().quantity;
				Couple <Integer,Integer> tmpcpl= new  Couple <Integer,Integer> (gold_amount,diam_amount);
				tmp_tresor_quantities_gold.put(ressource_r,tmpcpl);
			}
		}

		for (String agent : agents_sacs_gold){
			ArrayList<String> tmp_liste_gold =  new ArrayList<String>();
			Couple <String,List<String>> tmp_couple_gold = new Couple<String,List<String>>("Gold",tmp_liste_gold);
			this.bestSheet.put(agent,tmp_couple_gold);
		}

		for (String agent : agents_sacs_gold){
			List <String> tmp_liste=null;

			if (this.res_vitesse.get(agent)>1000){
				tmp_liste=ressources_map_gold;
			}else{
				tmp_liste=ressources_map_gold_reversed;
			}
			for (String tresor :tmp_liste){
				if(tresor==null) {
					continue;
				}
				if (tmp_tresor_quantities_gold.get(tresor).getLeft()>0){
					this.bestSheet.get(agent).getRight().add(tresor);
					int gain = min(tmp_cap_gold.get(agent).getLeft(),tmp_tresor_quantities_gold.get(tresor).getLeft());

					Couple<Integer,Integer > tmp_couple_gold=new Couple<Integer,Integer>(max(tmp_cap_gold.get(agent).getLeft()-gain,0),tmp_cap_gold.get(agent).getRight());
					
					scores_gold.put(agent,scores_gold.get(agent)+gain);

					Couple<Integer,Integer> tmp_cap_t = new  Couple<Integer,Integer>(max(tmp_tresor_quantities_gold.get(tresor).getLeft()-gain,0),tmp_tresor_quantities_gold.get(tresor).getRight());
					tmp_tresor_quantities_gold.put(tresor, tmp_cap_t);

					tmp_cap_gold.put(agent,tmp_couple_gold);
				}

				if (tmp_cap_gold.get(agent).getLeft()<=0){
					break;
				}
			}

		}
		
		recap_score(scores_gold);

	}



	private void recap_score(Hashtable<String,Integer> scores_gold){
		System.out.println(this.myagent.getLocalName()+" dit : score previsionnel : ");
		for (String agent : this.data.capacities.keySet()){
			if (scores_gold.get(agent)!=null) {
				System.out.println(agent+"   type :"+this.bestSheet.get(agent)+"   score: "+scores_gold.get(agent));
			}else {
				System.out.println(agent+"   type :"+this.bestSheet.get(agent));
			}
		}
	}


	public void action() {

		if (this.attente==-1){
			phase_init();
		}

		if (((this.data.capacities.keySet().size()  )>=this.list_agentNames.size()+1)&&(this.attente==0)){
			phase_1();
		}

		if (this.attente==1){
			phase_2();
			this.attente=2;
		}

		if (this.attente==2){
			this.attente=3;
			choisir_type();
			shareFeuillesDeRoute();
			System.out.println(this.myagent.getLocalName()+"    les feuilles de route des agents sont : "+this.bestSheet);
			this.data.feuille_de_route=this.bestSheet.get(this.myagent.getLocalName()).getRight();
			this.data.type=this.bestSheet.get(this.myagent.getLocalName()).getLeft();;
			this.data.feuilles_de_route=this.bestSheet;
		}

		if (this.attente==3 && this.data.interlocking==0){
			System.out.println(this.myagent.getLocalName()+"   je procede a la collecte");
			this.attente=4;
		}

		if (this.attente==4){
			this.myagent.doWait(5000);
			this.finished=1;
		}

		this.myAgent.doWait(100);
	}



	public int onEnd(){
		return finished;
	}

}